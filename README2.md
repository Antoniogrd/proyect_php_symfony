beercontroller.php

<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class BeerController extends AbstractController
/**
 * @Route("/beers/{id})
 */
{
    public function showBeers($id){

        return $this->render("/beers.html.twig", ["id"=>$id]);

    }
}

------------------
beers.html.twig

{% extends 'base.html.twig' %}

{% block stylesheets %}
        <link rel="stylesheet" href="{{asset('style/moviesPage.css')}}" type="text/css"/>
        <link href="{{ asset('styles/navbar.css') }}" type="text/css" rel="stylesheet"/>


{% endblock %}

{% block body %}

<div class="c-home">
            <h1>Cervezas</h1>

            {% for beer in beers %}
                <div>
                    <div class="c-home__beer">
                        <a href="{{path('detailPage', {id: beer.id}) }}"> 
                            <h3>{{beer.name}}</h3>
                            <p>{{beer.producer}}</p>
                            <p>{{beer.description}}</p>
                            <p>{{beer.graduation}}</p>
                            <img src="{{beer.image}}"/>
                        </a>
                    </div>
                </div>

            {% endfor %}
</div>

{% endblock %}


-------------
base.html.twig

    <!DOCTYPE html>
<html>
 
    <head>
         <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="preconnect" href="https://fonts.gstatic.com">
        <link href="{{ asset('styles/main.css') }}" type="text/css" rel="stylesheet"/>
        <link href="https://fonts.googleapis.com/css2?family=Roboto:ital,wght@0,400;0,700;1,100&display=swap" rel="stylesheet">
        {% block stylesheets %}
            
        {% endblock %}
        <title>Document</title>



        {% block javascripts %}
        {% endblock %}
    </head>
    
    <body>
        <div class="c-nav">
            <div class="c-nav__home">
                <a class="c-nav__link" href="{{path('moviesPage')}}"/>
                    <div>
                      <p>Inicio</p>
                    </div>
                </a>
            </div>

            <div class="c-nav__home">
                <a class="c-nav__link" href="{{path('moviesPage')}}"/>
                    <div>
                      <p>Cervezas</p>
                    </div>
                </a>
            </div>

            <div class="c-nav__home">
                <a class="c-nav__link" href="{{path('moviesPage')}}"/>
                    <div>
                      <p>Añadir</p>
                    </div>
                </a>
            </div>

            <div class="c-nav__home">
                <a class="c-nav__link" href="{{path('moviesPage')}}"/>
                    <div>
                      <p>Valóranos!</p>
                    </div>
                </a>
            </div>
        </div>
        
        {% block body %}{% endblock %}   
            
        
    </body>
    
</html>

---------------

insert into beer(Name, Producer, Description, Graduation, Image) values("DAMM INEDIT", "Damm", "Creada por los cerveceros de Damm junto a Ferrán Adriá y los sumilleres de El Bulli, se elabora con una mezcla de malta de cebada y trigo aromatizada con cilantro, piel de naranja y regaliz.", "4,8º", "https://shop.damm.com/214/cervezainedit.jpg");

-------------
Entrar en consola    docker-compose exec mysql bash
Entrar en mysql   mysql -u user -ppass demo

Primero tirar > select * from beer;
Query para editar

mysql> update beer set image="https://cdn.palbincdn.com/users/17395/images/cerveza-mahou-5-estrellas-botella-552-0829_3-1587557108.jpg" where image="https://elbardetucasa.com/wp-content/uploads/2020/05/10400-Botella_Mahou_5_Estrellas.jpg";
