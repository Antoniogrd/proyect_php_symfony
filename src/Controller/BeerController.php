<?php


namespace App\Controller;
// namespace App\Entity;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Beer;
use App\Manager\BeerManager;
use Doctrine\ORM\EntityManagerInterface;
use App\Form\BeerFormType;
use Symfony\Component\HttpFoundation\Request;

class BeerController extends AbstractController
{
    /**
     * @Route ("/")
     */
    public function hola()
    {
        return new Response("Hola mundo");
    }

    /**
     * @Route("/beers", name="showBeers")
     */
    public function showBeers(EntityManagerInterface $doctrine){
        {
            $repo = $doctrine->getRepository(Beer::class);

            $beers = $repo->findAll();
    
            return $this->render(
                "/beers.html.twig", 
                ["beers"=>$beers]);
    
        }
    }

    /**
     * @Route("/beer/create", name="createBeer")
     */
    public function createBeer(Request $request, EntityManagerInterface $doctrine)
    {
        $form = $this->createForm(BeerFormType::class);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $beer = $form->getData();

            $doctrine->persist($beer);
            $doctrine->flush();

            return $this->redirectToRoute("showBeers");
        }

        return $this->render(
            'insertBeer.html.twig',
            ['beerForm' => $form->createView()]
        );
    }



};